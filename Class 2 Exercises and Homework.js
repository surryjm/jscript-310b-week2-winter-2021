// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)

const myself = {
  firstName: 'Surry',
  lastName: 'Mowery',
  'favorite food': 'ice cream',
  bestFriend: {
  firstName: 'Ruby',
  lastName: 'Kwon',
  'favorite food': 'kimchi'} 
  }

console.log(myself);


// 2. console.log best friend's firstName and your favorite food

console.log(
`Best friend's first name: ${myself.bestFriend.firstName} 
My favorite food: ${myself["favorite food"]}`
);


// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X

let ticTacToe1 = 
[
  ['-', 'O', '-'],
  ['-', 'X', 'O'],
  ['X', '-', 'X']
];

console.log(ticTacToe1);


// 4. After the array is created, 'O' claims the top right square.
// Update that value.

let ticTacToe2 = JSON.parse(JSON.stringify(ticTacToe1));

ticTacToe2[0][2] = 'O';


// 5. Log the grid to the console.

console.log(ticTacToe2);


// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

const myEmail = 'surry@uw.edu';
const myEmail2 = 'foo@bar.baz';
const myEmail3 = 'surry@uw.e u';
const myEmail4 = 'surry@uw.ed u';

const regex = /[\w\d]+@([\w]+\.)+[\w][^-\s]+[^-\s]/;

console.log('Test 1: ' + regex.test(myEmail));
console.log('Test 2: ' + regex.test(myEmail2));
console.log('Test 3: ' + regex.test(myEmail3));
console.log('Test 4: ' + regex.test(myEmail4));


// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date

const assignmentDate = '1/21/2019';

const newDate = new Date(assignmentDate);

console.log(`Assignment date: ${newDate}`);


// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.

let dueDate = new Date(newDate.setDate(newDate.getDate() + 7));

console.log(`Due date: ${dueDate}`);


// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

let date = dueDate.getDate();
console.log(`Date: ${date}`);

let month = dueDate.getMonth();
console.log(`One digit month: ${month}`);
let monthStr = month.toString();
let twoDigitMonth = monthStr.padStart(2, '0')
console.log(`Two digit month: ${twoDigitMonth}`);

let year = dueDate.getFullYear();
console.log(`Year: ${year}`);


// 10. log this value using console.log

console.log(
  `<time datetime="${year}-${twoDigitMonth}-${date}">${months[0]} ${date}, ${year}</time>`
)